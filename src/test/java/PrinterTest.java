import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class PrinterTest {


    @Test
    public void test_getFiles(){

        String ipAddress = "10.4.2.22";
        FTPClient ftp = new FTPClient();
        try {
//           ftp.setPassiveLocalIPAddress(ipAddress);
            ftp.connect(ipAddress);
            ftp.login(null, null);
            ftp.enterLocalPassiveMode();
            ftp.setAutodetectUTF8(true);
            // ftp.login(userName, null, password);
            boolean noOptCommand = ftp.sendNoOp();
            ftp.setFileType(FTP.BINARY_FILE_TYPE);

            int replyCode = ftp.getReplyCode();
            int passivePort = ftp.getPassivePort();
            System.out.println("Reply code " + replyCode);
            if(!FTPReply.isPositiveCompletion(replyCode)) {
                ftp.disconnect();
                System.out.println("FTP server refused connection.");
            } else {
                FTPFile[] files = ftp.listFiles();
                System.out.println(files.length);
//               ftp.completePendingCommand();
                ftp.logout();

            }

            if(ftp.isConnected()) {
                try {
                    ftp.disconnect();
                } catch(IOException ioe) {
           ioe.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void printFile(){
        String ipAddress = "10.4.2.22";
        FTPClient ftp = new FTPClient();
        try {
//           ftp.setPassiveLocalIPAddress(ipAddress);
            ftp.connect(ipAddress);
            ftp.login(null, null);
            ftp.enterLocalPassiveMode();
            ftp.setAutodetectUTF8(true);
            // ftp.login(userName, null, password);
            boolean noOptCommand = ftp.sendNoOp();
            ftp.setFileType(FTP.BINARY_FILE_TYPE);

            int replyCode = ftp.getReplyCode();
            int passivePort = ftp.getPassivePort();
            System.out.println("Reply code {} " + replyCode);

            if(!FTPReply.isPositiveCompletion(replyCode)) {
                ftp.disconnect();
                System.out.println("FTP server refused connection.");
            } else {

                String path = getClass().getClassLoader().getResource("test.txt").getPath();

                File file = new File(path);
                FileInputStream fileIs = new FileInputStream(file);
                ftp.storeFile(file.getName(), fileIs);
                FTPFile[] files = ftp.listFiles();
                System.out.println(files.length);
                fileIs.close();
//               ftp.completePendingCommand();
                ftp.logout();

            }

            if(ftp.isConnected()) {
                try {
                    ftp.disconnect();
                } catch(IOException ioe) {
                    // do nothing
                }
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
